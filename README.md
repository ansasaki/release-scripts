# release-scripts

To update a Fedora package from an upstream release, follow the steps
below:

1. Make any packaging modifications in the Fedora package repository,
   other than the version bump (e.g., removing already upstreamed
   patches, adding a new patch)

2. Run the `make-release` script

The `make-release` script takes 3 arguments and options:

```
$ make-release [OPTIONS] PACKAGE VERSION TARGET
```

for example:
```
make-release gnutls 3.6.3 f29
make-release -b https://github.com/p11-glue/p11-kit/releases/download/0.23.14 -e gz p11-kit 0.23.14 master
```

The `-b` option specifies the base URL of the upstream release, where
a tarball and the signature are found.  By default, it picks the URLs
from the spec file.  The `-e` option specifies the filename extension
of the tarball (default "xz").
